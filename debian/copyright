Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gcstar
Upstream-Contact: Tian <tian@c-sait.net>
Source: http://www.gcstar.org/
Files-Excluded: share/gcstar/fonts packages

Files: *
Copyright: 2005-2016 Christian Jodar aka Tian <tian@gcstar.org, tian@c-sait.net>
           2005-2010 Bai Wensimi
           2005-2009 zserghei
           2007-2011 Petr Gajdůšek <gajdusek.petr@centrum.cz>
           2007-2009 Zuencap
           2007      Mattias de Hollander (MaTiZ) <mdehollander@gmail.com>
           2007      Yves Martin
           2008      t-storm
           2009-2010 Andrew Ross
           2009      FiXx
License: GPL-2+

Files: share/gcstar/helpers/*
Copyright: 2006      Kevin Krammer <kevin.krammer@gmx.at>
           2006      Jeremy White <jwhite@codeweavers.com>
License: Expat

Files: debian/*
Copyright: 2006-2011 Alexander Wirt <formorer@debian.org> 
           2014-2018 Jörg Frings-Fürst <debian@jff.email>
License: GPL-3+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 The complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
